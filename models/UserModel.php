<?php

namespace App\Models;

use App\Core\Field;
use App\Core\Model;
use App\Validators\BitValidator;
use App\Validators\DateTimeValidator;
use App\Validators\NumberValidator;
use App\Validators\StringValidator;

class UserModel extends Model
{
    protected function getFields (): array
    {
        return [
            'user_id'    => new Field((new NumberValidator())->setIntegerLength(11), false),
            'created_at' => new Field((new DateTimeValidator())->allowDate()->allowTime()),

            'username'   => new Field((new StringValidator())->setMinLength(2)->setMaxLength(30)),
            'password'   => new Field((new StringValidator())->setMinLength(3)->setMaxLength(120)),
            'email'      => new Field((new StringValidator())->setMinLength(2)->setMaxLength(30)),
            'first_name' => new Field((new StringValidator())->setMinLength(2)->setMaxLength(30)),
            'surname'    => new Field((new StringValidator())->setMinLength(2)->setMaxLength(30)),
            'is_active'  => new Field((new BitValidator())),
        ];
    }

    public function getByUsername (string $username)
    {
        $this->getByFieldName('username', $username);
    }


}