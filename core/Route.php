<?php

namespace App\Core;
class Route
{
    private $request_method;
    private $pattern;
    private $controller;
    private $method;

    private function __construct(string $request_method, string $patter, string $controller, string $method)
    {
        $this->request_method = $request_method;
        $this->pattern = $patter;
        $this->controller = $controller;
        $this->method = $method;
    }

    public static function get (string $patter, string $controller, string $method): Route
    {
        return new Route('GET', $patter, $controller, $method);
    }

    public static function post (string $patter, string $controller, string $method): Route
    {
        return new Route('POST', $patter, $controller, $method);
    }

    public static function any (string $patter, string $controller, string $method): Route
    {
        return new Route('GET|POST', $patter, $controller, $method);
    }

    public function matches (string $method, string $url): bool
    {
        if (!preg_match('/^' . $this->request_method . '$/', $method)) {
            return false;
        }

        return boolval(preg_match($this->pattern, $url));

    }

    public function getControllerName (): string
    {
        return $this->controller;
    }

    public function getMethodName (): string
    {
        return $this->method;
    }

    public function &extractArguments (string $url): array
    {
        $matches = [];
        $arguments = [];

        preg_match_all($this->pattern, $url, $matches);
        if (isset($matches[1])) {
            $arguments = $matches[1];
        }
        return $arguments;
    }
}