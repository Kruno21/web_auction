<?php

namespace App\Core;

class DBConnection 
{
    private $connection;
    private $configuration;

    public function __construct(DBConfig $dbConfig)
    {
        $this->configuration = $dbConfig;
    }

    public function getConnection(): \PDO
    {
        if ($this->connection == NULL) {
            $this->connection = new \PDO($this->configuration->getSourceString(),
                                        $this->configuration->getUser(),
                                        $this->configuration->getPass());
        }

        return $this->connection;
    }

}