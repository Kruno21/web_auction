<?php

namespace App\Controllers;

use App\Models\AuctionModel;
use App\Core\Controller;

class AuctionController extends Controller
{
    public function show($id)
    {
        $auctionModel = new AuctionModel($this->getDatabaseConnection());
        $auction = $auctionModel->getById($id);

        if (!$auction) {
            header('Location: /mvc_php_milan/');
            exit;
        }

        $this->set('auction', $auction);

        
        $offerModel = new \App\Models\OfferModel($this->getDatabaseConnection());
        $lastOfferPrice = $offerModel->getLastOfferPrice($auction);
        $this->set('lastOfferPrice', $lastOfferPrice);

        $auctionViewModel = new \App\Models\AuctionViewModel($this->getDatabaseConnection());

        $ipAddress = filter_input(INPUT_SERVER, 'REMOTE_ADDR');
        $userAgent = filter_input(INPUT_SERVER, 'HTTP_USER_AGENT');


        /*$auctionViewModel->add([
            'auction_id' => $id,
            'ip_address' => $ipAddress,
            'user_agent' => $userAgent
        ]);*/

    }


    private function normaliseKeywords(string $keywords): string
    {
        $keywords = trim($keywords);
        $keywords = preg_replace('/ +/', ' ', $keywords);

        return $keywords;
    }

    public function postSearch ()
    {
        $auctionModel = new \App\Models\AuctionModel($this->getDatabaseConnection());

        $q = filter_input(INPUT_POST, 'q', FILTER_SANITIZE_STRING);

        $keywords = $this->normaliseKeywords($q);

        $auctions = $auctionModel->getAllBySearch($keywords);

        $this->set('auctions', $auctions);
    }
}

?>