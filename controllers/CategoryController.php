<?php

namespace App\Controllers;

use App\Models\CategoryModel;
use App\Core\Controller;
class CategoryController extends Controller
{
    public function show ($id)
    {
        $categoryModel = new CategoryModel($this->getDatabaseConnection());
        $category = $categoryModel->getById($id);

        if (!$category) {
            header('Location: /mvc_php_milan/');
            exit;
        }
        $this->set('category', $category);

        $auctionModel = new \App\Models\AuctionModel($this->getDatabaseConnection());
        $auctionsInCategory = $auctionModel->getAllByCategoryId($id);

        $offerModel = new \App\Models\OfferModel($this->getDatabaseConnection());
        

        $auctionsInCategory = array_map(function($auction) use ($offerModel) {
            $auction->last_offer_price = $offerModel->getLastOfferPrice($auction);
            return $auction;
        }, $auctionsInCategory);

        $this->set('auctionsInCategory', $auctionsInCategory);

    }

    public function delete ($id)
    {
        die('Delete method');
    }
}