<?php

namespace App\Controllers;
use App\Models\AuctionModel;

class ApiAuctionController extends \App\Core\ApiController
{
    public function show ($id)
    {
        $auctionModel = new AuctionModel($this->getDatabaseConnection());
        $auction = $auctionModel->getById($id);
        $this->set('auction', $auction);
    }
}