<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Models\AuctionModel;
use App\Models\CategoryModel;
use App\Models\UserModel;
use App\Validators\StringValidator;

class MainController extends Controller
{
    public function home ()
    {
        $categoryModel = new CategoryModel($this->getDatabaseConnection());
        $categories = $categoryModel->getAll();
        $this->set('categories', $categories);


        /*$auctionModel = new AuctionModel($this->getDatabaseConnection());
        $auctionModel->add([
            'title' => 'Naslov aukcije 10',
            'description' => 'Ovo je opis aukcije',
            'starting_price' => '122.10',
            'starts_at' => '2020-01-04 20:15:00',
            'ends_at' => '2020-01-12 20:15:00',
            'is_active' => 1,
            'category_id' => 1
        ]); */
    }

    public function getRegister ()
    {

    }

    public function postRegister ()
    {
        $email       = filter_input(INPUT_POST, 'reg_email',      FILTER_SANITIZE_EMAIL);
        $first_name  = filter_input(INPUT_POST, 'reg_first_name', FILTER_SANITIZE_EMAIL);
        $surname     = filter_input(INPUT_POST, 'reg_surname',    FILTER_SANITIZE_EMAIL);
        $username    = filter_input(INPUT_POST, 'reg_username',   FILTER_SANITIZE_EMAIL);
        $password1   = filter_input(INPUT_POST, 'reg_password_1',  FILTER_SANITIZE_EMAIL);
        $password2   = filter_input(INPUT_POST, 'reg_password_2',  FILTER_SANITIZE_EMAIL);

        if ($password1 !== $password2) {
            $this->set('message', 'Niste unijeli dvije iste lozinke');
            return;
        }

        if (! (new StringValidator())->setMinLength(3)->setMaxLength(120)->isValid($password1)) {

            $this->set('message', 'Lozinka nije ispravnog formata');
            return;
        }

        $userModel = new UserModel($this->getDatabaseConnection());
        $user = $userModel->getByFieldName('email', $email);
        if ($user) {
            $this->set('message', 'Vec postoji korisnik s tim emailom');
            return;
        }

        $user = $userModel->getByFieldName('username', $username);
        if ($user) {
            $this->set('message', 'Vec postoji korisnik s tim username');
            return;
        }


        $passwordHash = password_hash($password1, PASSWORD_DEFAULT);


        $userId = $userModel->add([
            'username' => $username,
            'first_name' => $first_name,
            'surname' => $surname,
            'email' => $email,
            'password' => $passwordHash,
        ]);

        if (!$userId) {
            $this->set('message', 'Doslo je do pogreske: Niste se registrirali');
            return;
        }

        $this->set('message', 'Uspjesno ste se registrirali');
    }

    public function getLogin ()
    {

    }

    public function postLogin ()
    {
        $username = filter_input(INPUT_POST, 'login_username', FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, 'login_password', FILTER_SANITIZE_STRING);


        $validPassword = (new StringValidator())->setMinLength(3)->setMaxLength(120)->isValid($password);

        if (!$validPassword) {
            $this->set('message', 'Lozinika nije ispravnog formata');
            return;
        }

        $userModel = new UserModel($this->getDatabaseConnection());

        $user = $userModel->getByFieldName('username', $username);
        if (!$user) {
            $this->set('message', 'Ne postoji korisnik sa tim korisnickim imenom');
            return;
        }

        if(!password_verify($password, $user->password)) {
            sleep(1);
            $this->set('message', 'Lozinka nije ispravna');
            return;
        }

        $this->getSession()->put('user_id', $user->user_id);
        $this->getSession()->save();

        $this->redirect(\Configuration::BASE . 'user/profile');

    }

    public function getLogout(Type $var = null)
    {
        $this->getSession()->remove('user_id');
        $this->getSession()->save();

        $this->redirect(\Configuration::BASE);
    }


}