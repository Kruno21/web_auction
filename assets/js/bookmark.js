
function getBookmarks ()
{
    fetch(BASE + 'api/bookmarks', { credentials: 'include' })
        .then(result => result.json())
        .then(data => {
            displayBookmarks(data);
        })

}

function addBookmark (auctionId)
{
    fetch(BASE + 'api/bookmarks/add/' + auctionId, { credentials: 'include' })
        .then((response) => response.json())
        .then((data) => {
            if (data.error === 0){
                getBookmarks();
            }
        })
        .catch((error) => {
            console.error('Error:', error);
        });

}

function  clearBookmarks ()
{
    fetch(BASE + 'api/bookmarks/clear')
        .then(result => result.json())
        .then(data => {
            if (data.error === 0) {
                getBookmarks();
            }
        })
}
function displayBookmarks (bookmarks)
{
    const bookmarksDiv = document.querySelector('.bookmarks')
    bookmarksDiv.innerHTML = ''

    if (bookmarks.length === 0) {
        bookmarksDiv.innerHTML = 'No bookmarks';
        return
    }

    for (bookmark of bookmarks.bookmarks) {
        const bookmarkLink = document.createElement('a');
        bookmarkLink.style.display = 'block'
        bookmarkLink.style.marginTop = '10px'
        bookmarkLink.innerHTML = bookmark.title
        bookmarkLink.href = BASE + 'auction/' + bookmark.auction_id

        bookmarksDiv.appendChild(bookmarkLink)
    }
    //console.log(bookmarks.bookmarks)
}

addEventListener('load', getBookmarks)

